# lsm3d
## Tools for segmenting 3D images of plant tissues at multiple scales using the level set method

Copyright 2016 ENS de Lyon, see accompanying file LICENSE.txt

----

#### Authors :
Typhaine Moreau, Annamaria Kiss <annamaria.kiss@ens-lyon.fr.fr>
(Laboratoire Reproduction et Développement des Plantes, Univ Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRA, F-69342, Lyon, France)

----

### The "lsm3d" tools
* lsm_contour --> detects the outer surface of the tissue
* lsm_cells --> used to cellular segmentation or nuclei detection

----

### Dependencies

The levelset tools make use of the CImg image processing C++ library, which in turn needs Xlib library.
Furthermore, the `lsm_cells tool is parallelized using OpenMP.


### Download

----

```bash
git clone https://gitlab.inria.fr/gcerutti/lsm3d.git
```

### Pre-requisite: install `conda`

----

If `conda` is not already installed (open a terminal window and type `conda` to check), you may choose to install either [the miniconda tool](https://docs.conda.io/en/latest/miniconda.html) or [the anaconda distribution](https://docs.anaconda.com/anaconda/install/) suitable for your OS.


### Install from sources

----

```bash
cd lsm3d
conda env create -f conda/env/lsm3d.yaml
conda activate lsm3d
mkdir build
cd build
cmake ..
make install
```


### The following binaries will be installed in the environment:

----

* lsm_contour --> detects the outer surface of the tissue
* lsm_cells --> used to cellular segmentation or nuclei detection


#### lsm_contour: detects the outer surface of the tissue

* Usage : lsm_contour img t_up t_down a b smooth perUp perDown
Examples for parameter values:

```
img : grayscale image of cells, (.inr or .inr.gz)
Upper threshold : t_up = 20
Down threshold : t_down = 5
Area term : a = 0 (0.5, 1)
Curvature term : b = 0 (1)
Gaussian filter : smooth = 1 (0, if image already filtered)
Stop criteria : the contour evolution is in [perDown,perUp] for 10 consecutive iterations
     perUp = 0.002, perDown = -0.002`
```

* Test :

```
lsm_contour sample-stack.inr.gz 20 10 0 0 1 0.002 -0.002
```

#### lsm_cells: for cellular segmentation or nuclei detection

* Usage : lsm_cells img img_wat img_contour erosion [a b smooth lsm_type]

```
img : grayscale image of cells, (.inr or .inr.gz)
img_wat : image of seeds, (.inr or .inr.gz)
img_contour : mask, where cells do not evolve, (.inr or .inr.gz)
              if 'None', then cells can evolve on the whole image
erosion : amount of erosion of seeds for initialisation (uint8) --> -2, 0, 2
              if 0, then no erosion or dilation
              if negative, then a dilation is performed
a : area term (float) --> 0 or 0.5 or 1 (the default is 0.5)
              if negative, the object retracts
              if positive, the object inflates
b : curvature term (float) --> 0 or 1 (the default is 0)
gamma : scale parameter (float>0) --> 0.5 or 1 (the default is 1)
smooth : gaussian blur to apply to the image (int) --> 0 or 1 (the default is 0)
lsm_type : image, gradient or hessien based evolution --> 'i', 'g' or 'h' (the default is g)
```

* Test :

```
lsm_cells sample-satck.inr.gz sample-satck-wat.inr.gz 'None' 2 0.3 0 0.2 1 'h'
``` 
or
```
lsm_cells sample-satck.inr.gz sample-satck-wat.inr.gz sample-satck_LSMcont20-10a0b0s1/sample-satck_LSMcont20-10a0b0s1.inr.gz 2 0.3 0 0.2 1 'h'
```

