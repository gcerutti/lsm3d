import os
import subprocess

from setuptools import setup, find_packages, Extension
from setuptools.command.build_ext import build_ext


class CMakeExtension(Extension):
    def __init__(self, name, sourcedir=''):
        Extension.__init__(self, name, sources=[])
        self.sourcedir = os.path.abspath(sourcedir)


class CMakeBuild(build_ext):
    def run(self):
        try:
            out = subprocess.check_output(['cmake', '--version'])
        except OSError:
            raise RuntimeError("CMake must be installed!")

        for ext in self.extensions:
            self.build_extension(ext)

    def build_extension(self, ext):
        cmake_args = []
        env = os.environ.copy()
        for key in sorted(env.keys()):
            print('+ ' + key + ': ' + env[key])
        if 'PREFIX' in env:
            cmake_args += [f"-DCMAKE_INSTALL_PREFIX={env['PREFIX']}"]
            cmake_args += [f"-DCMAKE_PREFIX_PATH={env['PREFIX']}"]
        elif 'CONDA_PREFIX' in env:
            cmake_args += [f"-DCMAKE_INSTALL_PREFIX={env['CONDA_PREFIX']}"]
        cmake_args += ['-DCMAKE_INSTALL_LIBDIR=lib']

        if not os.path.exists("build/"):
            os.makedirs("build/")
        subprocess.check_call(['cmake', ext.sourcedir] + cmake_args, cwd="build/", env=env)
        subprocess.check_call(['make', '-j8', 'install'], cwd="build/")


short_descr = "Level-set methods for 3D images"
readme = open('README.md').read()

# find packages
pkgs = find_packages('python')

setup_kwds = dict(
    name='lsm3d',
    version="0.0.1",
    description=short_descr,
    long_description=readme,
    author="Annamaria Kiss",
    author_email="annamaria.kiss@ens-lyon.fr",
    url='https://gitlab.inria.fr/gcerutti/lsm3d',
    license='cecill-c',
    zip_safe=False,

    packages=pkgs,
    ext_modules=[CMakeExtension('src')],
    cmdclass={'build_ext': CMakeBuild},

    package_dir={'': 'python'},
    setup_requires=[],
    install_requires=[],
    tests_require=[],
    entry_points={},
    keywords='',

    test_suite='pytest',
)

# setup_kwds['entry_points']['console_scripts'] = []

setup(**setup_kwds)
