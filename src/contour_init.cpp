/*
Level-set Method to detect tissue contour (exterior shape)
- Sequential -

      Copyright 2016 ENS de Lyon

       File author(s):
           Typhaine Moreau, Annamaria Kiss <annamaria.kiss@ens-lyon.fr.fr>
       See accompanying file LICENSE.txt

To compile
 g++ -o contour_init contour_init.cpp -O2 -L/usr/X11R6/lib -lm -lpthread -lX11 -l:libtiff.so.5
 Need CImg.h and lsm_lib.h
 
To execute
 ./contour_init img t_up t_down smooth

 img : grayscale image of cells, .inr or .inr.gz
 t_up,t_down : linear threshold value (inr)
 smooth : amount of gaussian blur to apply to the image
*/

#include <iostream>
#include <math.h>
#include <sstream>
#include <vector>
#include <fstream>

#include "lsm_lib.h"

using namespace cimg_library;
using namespace std;

//------------------------------------------------------------------------------
//Main
//------------------------------------------------------------------------------
int main (int argc, char* argv[])
{
  clock_t begin=clock();

  if(argc!=5)
    {
      cout<<"!! wrong number of arguments"<<endl;
      cout<<"Usage : contour_init img t_up t_down smooth"<<endl;
      cout<<"Examples for parameter values:"<<endl;
      cout<<"------------------------------"<<endl;
      cout<<"img : grayscale image of cells, (.inr or .inr.gz)"<<endl;
      cout<<"Upper threshold : t_up = 20"<<endl;
      cout<<"Down threshold : t_down = 5"<<endl;
      cout<<"Gaussian filter : smooth = 1 (0, if image already filtered)"<<endl;
      return 0;
    }

  //ckeck filename and read image
  string filename=argv[1];
  float tailleVoxel[3] = {0};// resolution initialisation
  CImg<float> img=imread(filename, tailleVoxel); 

  //--------------------------------------------Parameters
  //linear threshold and smoothing
  int t_up=atoi(argv[2]);
  int t_down=atoi(argv[3]);
  float smooth=atof(argv[4]);

  //-------------------------------------------Names and directories
  //new name with arguments
  string ar2=argv[2];
  string ar3=argv[3];
  string ar4=argv[4];
  string insert="_initial"+ar2+"-"+ar3+"s"+ar4;
  filename.insert(filename.size()-4,insert);

  //-----------------------------------------Image Pre-processing
  //smooth image
  img.blur(smooth);

  //-------------------------------------------Initialization
  //initialize level-set
  CImg<unsigned char> segmented=threshold_linear_alongZ(img,t_up,t_down);
  int s=imsave(filename, tailleVoxel, segmented);
  return 0;
}
