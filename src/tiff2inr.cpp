/*
Convert tif file into inr

To compile :
 g++ -o tiff2inr tiff2inr.cpp -O2 -L/usr/X11R6/lib -lm -lpthread -lX11 -fopenmp -l:libtiff.so.5
 Needs CImg.h

To execute :
 ./tiff2inr img.tif compress

 img : image in .tif
 compress : if 0 then output image is not compressed (img.inr), else the output is img.inr.gz
 
*/
#include <iostream>
#include <math.h>
#include <sstream>
#include <fstream>

#define cimg_use_tiff
#include "CImg.h"

using namespace cimg_library;
using namespace std;

//-----------------------------------------------------------------------------
//Main
//-----------------------------------------------------------------------------
int main (int argc, char* argv[])
{
  
  if( argc>3 || argc<2 )
    {
      cout<<"tiff2inr : !! wrong number of arguments !!"<<endl;
      cout<<"Usage : tiff2inr img.tif compress"<<endl;
      return 0;
    }

// -------------------
// Sparsing arguments
// ------------------- 
  string name=argv[1];
  
  bool compress=true;
  if (argc==3)
	{
	if (string(argv[2])=="0")compress=false;
	cout<<"Output file will not be compressed. "<<endl;
	}
  
  CImg<> img;
  CImg<char> description;
  float tailleVoxel[3] = {0};  
  
  if(name.compare(name.size()-4,4,".tif")==0)
  {
	cout<<"The input file is a .tif file. OK"<<endl;   
  }
  else
  {
	cout<<"This is a tif file converter. Please give a .tif file as input."<<endl;
	return 0;
  }
  
// -----------------------
// Reading the .tif image file
// -----------------------
cout<<"File to read : "<<name<<endl;
  if(name.compare(name.size()-4,4,".tif")==0)
  {
	cout<<"The input file is a .tif file."<<endl;   
  }
  else
  {
	cout<<"This is a tif file converter. Please give a .tif file as input."<<endl;
	return 0;
  }
  
img.load_tiff(name.c_str(),0,~0U,1,tailleVoxel,&description);
cout<<"Image depth : "<<img.depth()<<endl;
cout<<"Voxel size : ("<<tailleVoxel[0]<<","<<tailleVoxel[1]<<","<<tailleVoxel[2]<<")"<<endl;


// -----------------------
// Writing the .inr or .inr.gz file
// -----------------------
	      
name.erase(name.size()-4);
string newname=name+string(".inr");
CImg<unsigned char> imgbis=img;
img.assign();
imgbis.save_inr(newname.c_str(),tailleVoxel);
imgbis.assign();

if (compress)
	{
		string zip="gzip -f "+newname;
		if(system(zip.c_str()));
	}

return 0;
}
